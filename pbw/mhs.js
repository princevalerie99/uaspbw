list_mhs = []

function saveMhs() {
    var mhs = []
    var nama = document.getElementById('nama').value
    var total = document.getElementById('total').value
    mhs['nama'] = nama
    mhs['total'] = total
    list_mhs.push(mhs)

    document.getElementById('data').innerHTML = "";

    var h = "<tr><th>MENU</th> <th>JUMLAH</th></tr>"
    for (i = 0; i < list_mhs.length; i++) {
        h += '<tr><td>'+list_mhs[i].nama+'</td><td>'+list_mhs[i].total+'</td></tr>'
    }
    document.getElementById('data').innerHTML = h
}

function deleteMhs() {
    // Menghapus data terakhir dari array
    list_mhs.pop();

    // Menghapus data sebelumnya
    document.getElementById('data').innerHTML = "";

    // Membuat tabel baru dengan data terbaru
    var h = "<tr> <th>MENU</th> <th>JUMLAH</th></tr>";
    for (i = 0; i < list_mhs.length; i++) {
        h += '<tr><td>'+list_mhs[i].nama+'</td><td>'+list_mhs[i].total+'</td></tr>';
    }
    document.getElementById('data').innerHTML = h;
}